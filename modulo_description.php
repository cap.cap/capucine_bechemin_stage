<div class="page-header page-header-bg" style="background-image: url('images/moduloequipe.jpg');">
    <div class="container">
        <a href="http://www.moduloplus.com/"><h1><span>Description</span>Modulo +</h1></a>
    </div><!-- End .container -->
</div><!-- End .page-header -->

<div class="about-section">
    <div class="container">
        <h2 class="title">Modulo +</h2>
        <p>Moduloplus est une entreprise de conseils et création de site web sur mesure.</p>
        <p>Depuis 2005, Régis Oliviero tiens cette entreprise en tant qu'indépendant. C'est quelques années plus tard qu'il décide de travailler dans un bureau ouvert avec d'autres indépendants. Ils fondent <a href="https://www.facebook.com/surton31.fr/">Sur Ton 31</a>. Cette équipe est donc composée de quatre personnes dont une graphiste/intégratrice, deux graphistes et lui-même, développeur. Leurs métiers étant complémentaires ils se recommandent auprès de leurs clients et sont amenés à partager des projets.</p>
    </div><!-- End .container -->
</div><!-- End .about-section -->

<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Quelques uns de ses projets</span>
        </h2>
    </div>
</div>

<div class="partners-container partners-container-alt">
    <div class="container">
        <div class="partners-carousel owl-carousel owl-theme">
            <a href="http://www.moduloplus.com/"><img src="images/modulologo.png" width="120" alt="Logo Modulo +"><p>Modulo +</p></a>
            <a href="https://www.luxuryprojects.fr/"><img src="images/modulosite1.jpg" width="120" alt="Luxyry Projects"><p>Luxyry Projects</p></a>
            <a href="http://www.stef-graphisme.com/"><img src="images/modulosite2.jpg" width="120" alt="Stephanie Villon"><p>Stephanie Villon</p></a>
            <a href="http://www.chabagny-racing.com/"><img src="images/modulosite3.jpg" width="120" alt="Chabagny Racing"><p>Chabagny Racing</p></a>
            <a href="https://www.menuiserie-orvault.com/"><img src="images/modulosite4.jpg" width="120" alt="Menuiserie Guiné Orvault"><p>Menuiserie Guiné Orvault</p></a>
        </div><!-- End .partners-carousel -->
    </div><!-- End .container -->
</div><!-- End .partners-container -->
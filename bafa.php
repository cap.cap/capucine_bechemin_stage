<div class="page-header page-header-bg" style="background-image: url('images/bafafond.jpg');">
    <div class="container">
        <a href="http://www.moduloplus.com/"><h1><span>La formation</span>B.A.F.A.</h1></a>
    </div><!-- End .container -->
</div><!-- End .page-header -->

<div class="container">
    <ul class="history-list">
        <li>
            <div class="thumb">
                <img src="images/1.png" alt="office">
            </div>
            <div class="featured-box">
                <div class="box-content">
                    <h2>Théorie</h2>
                    <p>Connaître les publics et les structures qui accueillent enfants et adolescents : séjours de vacances, accueils de loisirs..</p>
                    <p>Découvrir, préparer et animer des jeux, des chants, des activités,travailler en équipe, vivre et s'organiser en groupe</p>
                    <p>Plusieurs formules existent : pension complète, demi-pension ou externat. </p>
                </div>
            </div>
        </li>
        <li>
            <div class="thumb">
                <img src="images/2.png" alt="office">
            </div>
            <div class="featured-box">
                <div class="box-content">
                    <h2>Pratique</h2>
                    <p>C'est une étape forte de ton cursus, au sein d’une équipe et en situation réelle avec un groupe d'enfants. Il peut se faire en séjour de vacances comme en accueil de loisirs, dans tout type de structure dès lors qu'elle est déclarée auprès de la Direction Départementale de la Cohésion Sociale (DDCS ou DDCSPP de ton département).</p>
                    <p>Tu disposes de 18 mois après ta formation générale pour accomplir ce stage. C'est à toi d'accomplir les démarches pour trouver ce stage et être recruté.</p>
                </div>
            </div>
        </li>
        <li>
            <div class="thumb">
                <img src="images/3.png" alt="office">
            </div>
            <div class="featured-box">
                <div class="box-content">
                    <h2>Approfondissement</h2>
                    <p>C'est la dernière étape de ta formation, tu as le choix entre approfondir un thème ou te spécialiser sur une compétence sportive dans le cadre de la qualification. Cette étape ressemble dans la forme à la 1ère étape et s'articule autour de plusieurs objectifs :</p>

                    <p>l'analyse de ton stage pratique, la consolidation et l'enrichissement des acquis des étapes précédentes, notamment sur les aspects réglementaires et la découverte d'une thématique particulière.</p>
                    <p>Après cette formation, tu dois attendre la décision du jury Bafa qui valide ton cursus. Il y a un jury dans chaque département, il dépend de la Direction Départementale de la Cohésion Sociale [DDCS ou DDCSPP]. Le jury prend en compte les appréciations de chacun de tes stages pour donner sa décision.</p>
                </div>
            </div>
        </li>
    </ul>
    <p>Source : <a href="https://bafa.ufcv.fr/Le-BAFA/Les-3-etapes">UFCV</a></p>
</div>
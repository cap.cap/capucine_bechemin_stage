<div class="page-header page-header-bg" style="background-image: url('images/gruissan.jpg');">
    <div class="container">
        <h1><span>Gruissan</span>Gruiss'aventures</h1>
    </div><!-- End .container -->
</div><!-- End .page-header -->

<div class="about-section">
    <div class="container">
        <h2 class="title">Gruiss'aventures</h2>

        <h3>Les Moussaillons (6/9 ans)</h3>
        <p>Envie de vous éclater en pleine nature comme les vrais pirates de Barberousse ? Ce séjour est fait pour vous ! Durant 7 jours, sur le thème des Pirates Naufragés du Lagon, mixez l'apprentissage de la voile, la construction d'un radeau et une chasse au trésor.</p>

        <h3>Gruiss'aventures (9/11 ans)</h3>
        <p>L'aventure entre copains en voile, sur le sable, dans le massif de la Clape en VTT ou même à la découverte du vieux village de Gruissan. La garantie de ne pas s'ennuyer une seule seconde !</p>

        <h3>Multi'Gruissan (11/15 ans)</h3>
        <p>La proximité de l'immense plage de sable des Ayguades nous offre la possibilité de mixer sports nautiques en kayak et stand up paddle, et vitesse et pilotage en char à voile. Sensations garanties, baignades et mutliactivités sportives sur le sable chaud.</p>

        <h3>Multi'Beach (11/15 ans)</h3>
        <p>Les kilomètres de plage vierge et sauvage de La Vieille Nouvelle vous offrent le meilleur du vent et de la mer pour un séjour glisse et ambiance beach.</p>

        <h3>Natur'all camp (11/15 ans)</h3>
        <p>Gruissan, au bord des plages sauvages est aussi au coeur du Parc naturel de la Narbonnaise et de ses massifs montagneux idéaux pour les activités de pleine nature.</p>

        <h3>Entre mer et montagne (11/15 ans)</h3>
        <p>Une multitude d'activités sportives pour exploiter à fond toute la richesse du Parc naturel régional de la Narbonnaise, de ses espaces lagunaires et du massif de la Clape. </p>

        <h3>Gruissan Ride n'Raid (15/17 ans)</h3>
        <p>Une semaine intense d'activités sportives et de partage pour les plus autonomes. Journées continues avec pique-nique, raids en VTT, en stand up paddle, en catamaran avec bivouac sur la plage sauvage. Un séjour riche en émotions et en dépense physique pour une semaine sportive.</p>

    </div><!-- End .container -->
</div><!-- End .about-section -->

<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Les organisateurs</span>
        </h2>

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <a href="http://www.ucpa.com/">
                        <img src="images/ucpa.png" alt="member">
                        <h3 class="member-title">UCPA</h3>
                    </a>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .team-section -->

<div class="container">
    <h2 class="subtitle mb-3">
        <span>Bilan</span>
    </h2>
    <p>Le séjour UCPA se déroulait dans un camping à Gruissan. Notre camp accueillait 80 enfants de 6 à 17 ans pour 15 animateurs et une directice. Les enfants était regroupé dans des groupes d'activitées en fonction de leur ages. Chaque animateurs avait une fonction particulière par jour. Etant stagiaire il m'a été attribué un groupe avec une autre animatrice: les moussaillons (8 enfants de 6 à 9ans). Je les ait donc accompagné dans leur vie quotidienne et activités durant toute la semaine. Cepandant j'ai éé aussi resposable d'autres groupe deux après midi seule et avec d'autre animateurs pour des jeunes plus agé, de 9 à 11ans et de 11 à 15ans.</p>
    <p>Cette troisième semaine de stage ne m'a pas satisfaite, en effet j'ai mit du temps à m'habituer au fonctionnement du centre. L'organisation et la communication manquait parfois entre l'équipe. C'est une experience enrichissante pour la suite mais je ne souhaite pas la renouveler. En tant qu'animatrice, je trouve ça complexe de faire vivre un très bon séjour à 80 jeunes en une semaine. On arrive pas à connaitre tout le monde et on se fait bloquer par les autres groupes que ce soit du point de vue du matériel, pour des animations, ou du quota d'anims qui peut être trop faible sur le camps pour amener un groupe en sortie. Un petit séjour (20 jeunes) est plus convivial et permet de plus réaliser les envies des jeunes.</p>
    <p>La directrice à sentie que ce concept ne me convenait pas mais a su noter mon investissement au près des enfants et du camp.</p>
</div>


<div class="container">
    <div class="footer-bottom">
        <div class="row col-md-12">

            <div class="col-md-6">
                <p class="footer-copyright">e-mail : <br>capucinebechemin@gmail.com</p>
            </div>

            <div class="social-icons">
                <a href="https://www.linkedin.com/in/capucine-bechemin-b7060a183/" class="social-icon" target="_blank"><i class="icon-linkedin"></i></a>
                <a href="https://www.instagram.com/capucine.bcn/" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
            </div><!-- End .social-icons -->

        </div>

    </div><!-- End .footer-bottom -->


</div><!-- End .containr -->
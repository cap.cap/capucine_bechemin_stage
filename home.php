<div class="home-top-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="banner banner-image">
                    <a href="?page=cv.php">
                        <img src="images/cvhome.png" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-lg-5 -->

            <div class="col-lg-7 top-banners">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="banner banner-image">
                            <a href="?page=modulo_projets.php">
                                <img src="images/projethome.JPG" alt="banner">
                            </a>
                        </div><!-- End .banner -->
                    </div><!-- End .col-sm-6 -->
                    <div class="col-sm-6">
                        <div class="banner banner-image">
                            <a href="?page=modulo_description.php">
                                <img src="images/modulohome.JPG" alt="banner">
                            </a>
                        </div><!-- End .banner -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->
                <div class="col-sm-12">
                    <div class="banner banner-image">
                        <a href="?page=bafa.php">
                            <img src="images/bafahome.png" alt="banner">
                        </a>
                    </div><!-- End .banner -->
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="banner banner-image">
                            <a href="?page=andar_sejour.php">
                                <img src="images/fermehome.png" alt="banner">
                            </a>
                        </div><!-- End .banner -->
                    </div><!-- End .col-sm-6 -->
                    <div class="col-sm-6">
                        <div class="banner banner-image">
                            <a href="?page=gruissan_sejour.php">
                                <img src="images/gruissanhome.png" alt="banner">
                            </a>
                        </div><!-- End .banner -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->
            </div><!-- End .col-lg-7 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .home-top-container -->
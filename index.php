
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Capucine Bechemin</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Bootstrap eCommerce Template">
    <meta name="author" content="SW-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="images/logo.jpg">

    <link rel="stylesheet" href="demo-11/assets/css/bootstrap.min.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="demo-11/assets/css/style.min.css">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="demo-7/assets/css/bootstrap.min.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="demo-7/assets/css/style.min.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="style_modif.css">

</head>
<body>
<div class="page-wrapper">
    <header class="header">
        <?php
        require_once 'header.php';
        ?>
    </header>
    <main class="main">
        <?php
        $page = (isset($_GET['page']))?$_GET['page']:'home.php';
        require_once ''.$page.'';
        ?>
    </main>
    <footer class="footer">
        <?php
        require_once 'footer.php';
        ?>
    </footer>
</div>

<?php
    require_once 'overlay_menu.php';
?>

<script src="demo-7/assets/js/jquery.min.js"></script>
<script src="demo-7/assets/js/bootstrap.bundle.min.js"></script>
<script src="demo-7/assets/js/plugins.min.js"></script>

<script src="demo-7/assets/js/main.min.js"></script>


</body>
</html>

<div class="page-header page-header-bg" style="background-image: url('images/chant.jpg');">
    <div class="container">
        <h1><span>C.V.</span>Capucine Bechemin</h1>
    </div><!-- End .container -->
</div><!-- End .page-header -->


<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Formations & Diplômes</span>
        </h2>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row row-sm">
                        <div class="col-5 col-md-3 col-xl-2">
                            <div class="product">
                                <figure class="product-image-container">
                                    <a class="product-image">
                                        <img src="images/html.jpg" alt="HTML">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:90%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->
                                    </div><!-- End .product-container -->
                                </div><!-- End .product-details -->
                            </div><!-- End .product -->
                        </div><!-- End .col-xl-3 -->

                        <div class="col-5 col-md-3 col-xl-2">
                            <div class="product">
                                <figure class="product-image-container">
                                    <a class="product-image">
                                        <img src="images/css.jpg" alt="CSS">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->
                                    </div><!-- End .product-container -->
                                </div><!-- End .product-details -->
                            </div><!-- End .product -->
                        </div><!-- End .col-xl-3 -->

                        <div class="col-5 col-md-3 col-xl-2">
                            <div class="product">
                                <figure class="product-image-container">
                                    <a class="product-image">
                                        <img src="images/php.jpg" alt="PHP">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:70%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->
                                    </div><!-- End .product-container -->
                                </div><!-- End .product-details -->
                            </div><!-- End .product -->
                        </div><!-- End .col-xl-3 -->

                        <div class="col-5 col-md-3 col-xl-2">
                            <div class="product">
                                <figure class="product-image-container">
                                    <a class="product-image">
                                        <img src="images/sql.jpg" alt="SQL">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->
                                    </div><!-- End .product-container -->
                                </div><!-- End .product-details -->
                            </div><!-- End .product -->
                        </div><!-- End .col-xl-3 -->

                        <div class="col-5 col-md-3 col-xl-2">
                            <div class="product">
                                <figure class="product-image-container">
                                    <a class="product-image">
                                        <img src="images/python.jpg" title="Python">
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->
                                    </div><!-- End .product-container -->
                                </div><!-- End .product-details -->
                            </div><!-- End .product -->
                        </div><!-- End .col-xl-3 -->
                    </div>
                </div>
            </div>
        </div>
        <p>Ecole Ynov ingesup, Nantes. Première année d'informatique</p>
        <p>Anglais L.V.1. niveau B1 à B2</p>
        <p>Espagnol L.V.2. niveau A2 à B1</p>
        <p>Formation B.A.F.A.</p>
        <p>Lycée Saint Dominique, Saint-Herblain : Cursus général scientifique Sciences de la Vie et de la Terre, spécialitée mathémathiques, option art plastique</p>

    </div><!-- End .container -->
</div><!-- End .about-section -->

<div class="partners-container partners-container-alt">
    <div class="container">
        <div class="partners-carousel owl-carousel owl-theme">
            <img class="diplomes" src="images/first.jpg" width="120" title="First certificate of Cambridge">
            <img class="diplomes" src="images/bac.jpg" width="120" title="Baccalauréat">
            <img class="diplomes" src="images/bafa.jpg" width="120" title="BAFA">
            <img class="diplomes" src="images/permisb.png" width="120" title="Permis B">
            <img class="diplomes" src="images/psc.jpg" width="120" title="Brevet de secourisme niveau 1">
        </div><!-- End .partners-carousel -->
    </div><!-- End .container -->
</div><!-- End .partners-container -->

<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Musique</span>
        </h2>

        <p>→ Pratique du chant depuis 5 ans en école de musique</p>
        <p>→ Formation d'un groupe de musique dont je suis la chanteuse depuis 2015 <a href="https://www.instagram.com/_feelingred/">Feeling Red</a></p>
        <p>→ Apprentissage de la guitare folk en autodidacte puis cours de guitare électrique durant l'année 2017</p>
        <p>→ Apprends différents instruments en autodidacte ; batterie, ukulele, piano, et plus récement violon</p>
       
    </div>
</div>

<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Voyager</span>
        </h2>

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <img src="images/temple_thai.jpg" alt="member">
                    <h3 class="member-title">Thailande</h3>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <img src="images/antilles.jpg" alt="member">

                    <h3 class="member-title">Guadeloupe Martinique</h3>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <img src="images/afriquedusud.jpg" alt="member">

                    <h3 class="member-title">Afrique du Sud</h3>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <img src="images/bus_americain.jpg" alt="member">

                    <h3 class="member-title">Etat unis</h3>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .team-section -->

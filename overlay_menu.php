
<div class="mobile-menu-overlay"></div>

<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                    <li><a href="index.php">Home</a></li>

                    <li><a href="?page=modulo_description.php" class="sf-with-ul">Stage Modulo +</a>
                        <ul>
                            <li><a href="?page=modulo_description.php">Description</a></li>
                            <li><a href="?page=modulo_projets.php">Mes projets</a></li>
                        </ul>
                    </li>

                    <li><a href="?page=bafa.php" class="sf-with-ul">Stage Bafa</a>
                        <ul>
                            <li><a href="?page=andar_sejour.php">Andar</a></li>
                            <li><a href="?page=gruissan_sejour.php">Gruissan</a></li>
                        </ul>
                    </li>

                    <li><a href="?page=cv.php">Capucine Bechemin</a></li>
            </ul>
        </nav><!-- End .mobile-nav -->
    </div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->

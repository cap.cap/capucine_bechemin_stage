<div class="page-header page-header-bg" style="background-image: url('images/andar.jpg');">
    <div class="container">
        <h1><span>Andar</span>Ferme pédagogique La clé des Champs</h1>
    </div><!-- End .container -->
</div><!-- End .page-header -->

<div class="about-section">
    <div class="container">
        <h2 class="title">La clé des champs (6/9 ans)</h2>
        <p>La ferme n'aura plus de secrets pour vous.
            Les veaux, les lapins, les poules, les moutons
            « tricot », « cheepy » et « curly », sans oublier
            l’âne « shreck » et le poney « boby », seront
            aux petits soins. Vous découvrirez la traite des
            vaches et appendrez à transformer le lait en
            différents produits laitiers. Vos journées seront
            rythmées par la vie de la ferme avec le chant
            du coq, le meuglement des vaches, les activités
            et les veillées concoctées par l’équipe.</p>
    </div><!-- End .container -->
</div><!-- End .about-section -->

<div class="team-section">
    <div class="container">
        <h2 class="subtitle mb-3">
            <span>Les organisateurs</span>
        </h2>

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <a href="http://www.ferme-andard.com/">
                        <img src="images/lacledeschamps.jpg" alt="member">
                        <h3 class="member-title">La clé des champs</h3>
                    </a>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-3">
                <div class="member">
                    <a href="https://www.saint-herblain.fr/">
                        <img src="images/mairiesh.jpg" alt="member">
                        <h3 class="member-title">Mairie de St Herblain</h3>
                    </a>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .team-section -->

<div class="container">
    <h2 class="subtitle mb-3">
        <span>Bilan</span>
    </h2>
    <p>En amont de ce séjour, j'ai participé à des réunions de préparation où nous avons pu établir un projet pédagogique et réaliser quelques affiches à destination des enfants comme l'emploi du temps ou la roue des tâches. Nous avons également discuté des activités et veillées que nous voudrions organiser. La directice du séjour à finalement été remplacée pour cause de grossesse.</p>
    <p>Ce stage s'est effectuée en deux semaines de 5 jours avec un public de 6 à 9 ans et une équipe d'animation de 3 personnes. Lors de ce stage j'ai pu comprendre le role de l'animateur et les besoins quotidien des enfants. Ce fut une très belle experience d'animation et de vie.</p>
    <p>La directrice à apprécié mon investissement et à validé mes 10 jours.</p>
</div>
<div class="page-header page-header-bg" style="background-image: url('images/moduloequipe.jpg');">
    <div class="container">
        <a href="http://www.moduloplus.com/"><h1><span>Projets</span>Modulo +</h1></a>
    </div><!-- End .container -->
</div><!-- End .page-header -->


<div class="team-section">
    <div class="container">
        <h1>Le Comptoir des Savonniers</h1>

        <p><strong>Le Comptoir des Savonniers</strong> est une chaîne de vente de savons. Il existe plusieurs points de vente en France et à l'étranger. Ils ne dépendent pas de la marque et gèrent eux-mêmes leur communication et vente. En 2011, Modulo + avait réalisé le site internet de <strong>la boutique de Guérande</strong>.</p>

        <h2 class="subtitle mb-3">
            <span>Intégration</span>
        </h2>
        <p>Lors de ce stage ma mission était de rafraichir ce site. En effet, comme vous pouvez le voir le design était très vieux et dépassé. Nous avons utilisé un template e-commerce sur <a href="https://portotheme.com/html/porto_ecommerce/">Porto</a>. Celui-ci nous a permis de gagner beaucoup de temps et d'obtenir un résultat propre et responsive. Cependant, il a parfois été complexe de le comprendre.</p>
        <p>Voici le résultat en image de cette intégration (toutes les pages ne sont pas présentées ici).</p>
        <h2 class="subtitle mb-3">
            <span>Intégration : Résultat</span>
        </h2>
        <div class="product-single-container product-single-default">
                            <div class="product-slider-container product-item">
                                <div class="product-single-carousel owl-carousel owl-theme">

                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirnouveau.JPG" data-zoom-image="assets/images/products/zoom/product-1-big.jpg"/>
                                    </div>
                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirancien.JPG" data-zoom-image="assets/images/products/zoom/product-1-big.jpg"/>
                                    </div>

                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirnouveau4.JPG" data-zoom-image="assets/images/products/zoom/product-2-big.jpg"/>
                                    </div>
                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirancien4.JPG" data-zoom-image="assets/images/products/zoom/product-2-big.jpg"/>
                                    </div>

                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirnouveau2.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>
                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirancien2.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>

                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirnouveau5.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>
                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirancien5.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>

                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirnouveau6.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>
                                    <div class="product-item">
                                        <img class="product-single-image" src="images/comptoirancien6.JPG" data-zoom-image="assets/images/products/zoom/product-4-big.jpg"/>
                                    </div>
                                </div><!-- End .product-single-carousel -->
                            </div>
                </div>

        <h2 class="subtitle mb-3">
            <span>SQL | PHP</span>
        </h2>

        <p>A la suite de cette partie d'intégration, j'ai pu explorer le PHP avec la base de données. Pour cela, il a fallu que j'injecte le script de la base du site dans une base en local pour pouvoir l'exploiter sans faire de mauvaise manipulation qui pourrait faire perdre beaucoup de temps. Avec cette base je devais afficher les savons sur différentes pages :
            <ul>
                <li><strong>° Affichage des savons sur la page de leur catégorie avec le passage de l'id rubrique en paramètre :</strong></li>
                    <p>On a un fichier PHP pour toutes les catégories.</p>
                    <img src="images/li1.JPG" class="ImageCode">

                <li><strong>° Affichage de la page du produit précis avec le passage de son id en paramètre reprenant ses détails :</strong></li>
                    <p>Récuération des informations du produit cible, de sa rubrique et de sa photo ainsi que des produits à la Une. Tout ça est aussi géré dans un seul fichier PHP, l'id produit nous permet de prendre les informations qui lui correspondent dans la ligne de la table produit.</p>
                    <img src="images/li2.JPG" class="ImageCode">
                    <p>Affichage du nom, de la description et de la composition du produit.</p>
                    <img src="images/li21.JPG" class="ImageCode">
                    <img src="images/li22.JPG" class="ImageCode">
                    <img src="images/li23.JPG" class="ImageCode">
                    <p>Affichage des photos qui sont dans une autre table en faisant le lien avec l'id du produit correspondant.</p>
                    <img src="images/li3.JPG" class="ImageCode">

                <li><strong>° Affichage des bannières précises sur les photos si le produit est Nouveau, Coup de Coeur ou Top des Ventes :</strong></li>
                    <p>On suit le même principe pour le flag à la Une.</p>
                    <img src="images/li4.JPG" class="ImageCode">

        </ul>
        </p>
        <h2 class="subtitle mb-3">
            <span>SQL | PHP : Résultat</span>
        </h2>

        <div class="row">
            <div class="col-sm-6 col-lg-4">
                <div class="member">
                    <img src="images/affichebdd.JPG" alt="member">
                    <h3 class="member-title">Tous les produits tonifiants</h3>

                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-4">
                <div class="member">
                    <img src="images/affichebdd2.JPG" alt="member">
                    <h3 class="member-title">A la Une</h3>

                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->

            <div class="col-sm-6 col-lg-4">
                <div class="member">
                    <img src="images/affichebdd3.JPG" alt="member">
                    <h3 class="member-title">Détail du produit : Rubrique, Nom, Description, Composition, Image</h3>
                </div><!-- End .member -->
            </div><!-- End .col-lg-3 -->
        </div><!-- End .row -->

        <h1>API Facebook et Google</h1>

        <p>Cette deuxième mission, plus courte, consistait à faire des recherches sur l'utilisation des API Facebook et Google. Une API (interface de programmation d’application) permet de faire communiquer un logiciel avec un autre en partageant leur services.</p>
        <p>Un client souhaiterai ajouter des fonctionnalitées qui font appel aux API Facebook et Google. Son entreprise <a href="http://www.cotton-quiz.com/" >Cotton Quiz</a> permet de générer des quiz lors de soirées bar.</p>

        <h2 class="subtitle mb-3">
            <span>Facebook</span>
        </h2>
        <div class="row">
            <div class="col-4">
                <img width="200" class="api_images" src="images/facebook.png">
            </div>
            <div class="col-8">
                <p>Il souhaite que dès lors qu'un des utilisateurs de son site crée une soirée quiz, il se publie automatiquement un évènement Facebook sur la page de Cotton Quiz.</p>
                <p>Suite à de multiples recherches et essais j'ai pu réussir à établir une connexion entre mon site et mon compte Facebook. Ma page PHP me renvoie : Logged as Capucine Bcn.</p>
                <p>Ces recherches furent longue et pleine d'erreurs qu'il fallait comprendre et résoudre. En effet la mise en place de l'API Facebook pour les évènements n'est pas très bien renseignée et il est difficile de s'y retrouver dans toutes les étapes qu'elle contient.</p>
            </div>
        </div>

        <h2 class="subtitle mb-3">
            <span>Google</span>
        </h2>

        <div class="row">
            <div class="col-4">
                <img width="200" class="api_images" src="images/search.png">
            </div>
            <div class="col-8">
                <p>Il souhaite également qu'il y ait un raccourci permettant aux visiteurs d'ajouter l'évènement à leur google agenda.</p>
                <p>Google a quant à lui mit en place un site officiel bien plus explicite sur l'utilisation de ses API, on y trouve même une vidéo explicative de la mise en place de l'ajout d'un évènement à Google Agenda en Java.</p>
                <p>Ce site nous donne un tutorial à suivre de quatre étapes. Le stage s'étant terminé je n'ai pas pu conclure et me suis arrêtée à la mi-chemin de cette ultime étape. Je n'ai pas obtenu de résultat puisque les étapes que j'ai suivies permettent avant tout de paramétrer cette connexion.</p>
            </div>
        </div>

        <h1>Bilan</h1>

        <p>Ce stage m'a apporté des compétences techniques, mais aussi une connaissance de la réalité de la relation client et du déroulement d'un projet.</p>
        <p>L'utilisation d'un template était nouveau pour moi, je n'ai pas rencontré de gros soucis de compréhension excepté dans les noms des classes. En effet, le css étant très grand il était parfois difficile de trouver comment changer un aspect graphique d'un élément particulier.</p>
        <p>La suite du projet demandais plus de compétence en SQL et PHP, il fallait afficher les éléments depuis la base de données. Ce concept ne m'était pas étrangé suite aux projets de B1. Cependant il a fallut se plonger dans la base de donnée réalisée en 2011 et comprendre son fonctionnement, les liens entre les tables, les informations importantes etc. Ensuite, dans le code, j'ai appris une nouveau concept qui est de passer des paramètres dans l'url afin de les exploiter dans une autre page. Cela m'a permis d'avoir un seul fichier PHP qui traite l'affichage de toutes les fiches produits. Cette deuxième partie à donc été plus technique, je l'ai trouvé plus intéressante, avec un plus grand challenge.</p>
        <p>La recherche sur les APIs m'a permis de mieux comprendre leur utilité et de commencer à appréhender leur mise en place, c'est un concept qui me sera sûrement très utile plus tard.</p>
        <p>Durant les deux semaines j'ai pu observer la manière dont travail les indépendants. Etant seuls aux commandes, ils ont beaucoup plus de casquettes qu'un simple employé. Ils m'ont également donné quelques conseils notamment sur l'importance du cahier des charges et des conséquences qu'il peut engendrer s'il n'a pas été correctement rédigé. Leurs métiers étant complémentaires j'ai pu les observer échanger sur un projet commun, les métiers doivent s'adapter les uns aux autres pour un résultat harmonieux.</p>
        <p>Cela était très enrichissant et m'a montré la réalité du métier.</p>
    </div>
</div>